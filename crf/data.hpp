#pragma once

#include <random>

#import "graph.hpp"
#include "../util/util.hpp"

namespace crf {

    template<class IntDistribution = std::uniform_int_distribution<int>>
    auto donut(int dim_x, int dim_y, int reflabel_count)
        -> graph
    {
        auto g = graph{};

        // Allocate a grid of nodes and set each nodes id to the position within
        // the array.
        IntDistribution dis(0, reflabel_count - 1);
        g.nodes.reserve(dim_x * dim_y);
        for (auto i = 0; i < dim_x * dim_y; ++i) {
            g.nodes.push_back(node{
                .id = i,
                .reference_label = dis(util::mt19937),
                .assigned_label = 0,
                .observation = 0
            });
        }

        // Helper function to set the pointers to set an edge incident to
        // two nodes.
        auto init_edge = [](edge& e, node& head, node& foot) {
            e.head = &head;
            e.foot = &foot;
            head.edges.push_back(&e);
            foot.edges.push_back(&e);
        };

        // Allocate the edges and initialize them.
        g.edges.resize(2 * dim_x * dim_y);
        util::for_each(g.nodes, [&, i = 0](auto& node) mutable {
            // Add an edge from the current node to the next node on the right.
            auto id = node.id + 1;
            if (id % dim_x == 0)
                id -= dim_x;
            init_edge(g.edges[i], node, g.nodes[id]);
            i++;

            // Add an edge from the current node to the node below.
            id = node.id + dim_x;
            if (id >= dim_x * dim_y)
                id -= dim_x * dim_y;
            init_edge(g.edges[i], node, g.nodes[id]);
            i++;
        });

        return g;
    }

} // namespace crf
