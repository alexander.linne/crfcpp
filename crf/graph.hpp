#pragma once

#include <iomanip>
#include <map>
#include <queue>
#include <vector>

#include "basic_types.hpp"
#include "../util/util.hpp"

namespace crf {

    struct graph
    {
        auto compute_score() const
            -> double
        {
            auto sum = 0.0;

            util::for_each(nodes, [&](auto& node) {
                sum += node.scores[node.assigned_label];
            });
            util::for_each(edges, [&](auto& edge) {
                sum += edge.scores[edge.head->assigned_label]
                [edge.foot->assigned_label];
            });

            return sum;
        }

        void create_reference_label_random_walk(
            const std::vector<std::vector<double>>& transition_matrix,
            int iterations)
        {
            auto current_idx = util::random_index(nodes.size());
            do {
                // Select a neighbour.
                auto& cur_node = nodes[current_idx];
                auto neighbours = cur_node.neighbours();
                auto next_idx = *std::next(neighbours.begin(),
                    util::random_index(cur_node.edges.size()));

                // Assign a reference label depending to the neighbour depending
                // on the reference label of the current node.
                auto& transition_vector =
                    transition_matrix[nodes[current_idx].reference_label];
                nodes[next_idx].reference_label = static_cast<int>(
                    util::weighted_random_index(transition_vector));

                // Continue with the next node as current node.
                current_idx = next_idx;
            } while (iterations-- > 0);
        }

        void create_reference_label_patches(
            const std::vector<std::vector<double>>& transition_matrix,
            int iterations)
        {
            util::for_each(nodes, [&](auto& node) {
                node.reference_label =
                    std::uniform_int_distribution{0, 2}(util::mt19937);
            });

            while (iterations-- > 0) {
                // Select an node and neighbour.
                auto idx = util::random_index(nodes.size());
                auto neighbours = nodes[idx].neighbours();
                //auto next_idx = util::random_index(neighbours.size());


            }
        }

        void add_observation(
            std::vector<std::vector<double>> state_to_observation)
        {
            util::for_each(nodes, [&](auto& node) {
                node.observation = static_cast<int>(util::weighted_random_index(
                    state_to_observation[node.reference_label]));
            });
        }

        auto size() const noexcept
            -> size_t
        {
            return nodes.size();
        }

        auto operator[](size_t index)
            -> node&
        {
            return nodes[index];
        }

        auto operator[](size_t index) const
            -> const node&
        {
            return nodes[index];
        }

        std::vector<node> nodes;
        std::vector<edge> edges;
    };

    auto bfs_indices(const graph& g, int start_id)
        -> cppcoro::generator<int>
    {
        auto queue = std::queue<int>{};
        auto visited = std::vector<bool>(g.nodes.size(), false);
        queue.push(start_id);
        visited[start_id] = true;
        while (!queue.empty()) {
            auto current = queue.front();
            queue.pop();
            co_yield current;
            for (auto& node_id : g.nodes[current].neighbours()) {
                if (!visited[node_id]) {
                    queue.push(node_id);
                    visited[node_id] = true;
                }
            }
        }
    }

    void viterbi(const graph& g) {
        auto n = g.size();
        auto start_time = util::now();

        using util::indices;

        enum class set : int_fast8_t { border, inner, outer };
        struct labeling {
            labeling() : labeling({}, std::numeric_limits<double>::lowest()) {}
            labeling(std::vector<int> labels, double score)
                : labels{std::move(labels)}, score(score), to_add_score{0} {}
            std::vector<int> labels;
            double score;
            double to_add_score;
        };
        struct step {
            std::vector<set> belongs_to;
            std::map<std::vector<int>, labeling> labelings;
        };

        const auto has_outer_neighbour =
            [&g](int node_id, int next_to_add, const std::vector<set>& belongs_to) {
                auto outer_neighbours =
                    util::as_<std::vector<int>>(g[node_id].neighbours());
                for (auto& id : g[node_id].neighbours())
                    if (belongs_to[id] == set::outer && id != next_to_add)
                        return true;
                return false;
            };

        const auto calculate_next =
            [&n, &has_outer_neighbour](
                int next_to_add, const std::vector<set>& belongs_to) {
                auto next_belongs_to = belongs_to;
                auto next_to_move_size = size_t{0};
                auto next_border_size = size_t{0};
                for (auto i : util::indices(n)) {
                    if (belongs_to[i] == set::border
                        && !has_outer_neighbour(i, next_to_add, belongs_to)) {
                        next_to_move_size++;
                        next_belongs_to[i] = set::inner;
                    }
                    if (next_belongs_to[i] == set::border)
                        next_border_size++;
                }

                // Add the new element to the border.
                next_belongs_to[next_to_add] = set::border;
                next_border_size++;

                return std::tuple{next_belongs_to,
                    next_border_size, next_to_move_size};
            };

        const auto make_vector = [](size_t size, int init) {
            auto v = std::vector<int>(size);
            util::fill(v, init);
            return v;
        };
        auto cur = step { .belongs_to = std::vector<set>(n, set::outer) };
        cur.labelings[std::vector<int>{}] = labeling{make_vector(n, -1), 0.0};

        const auto order = util::as_<std::vector<int>>(
            bfs_indices(g, util::random_index(static_cast<int>(n))));
        std::cout << "order  ";
        for (auto e : order)
            std::cout << e << " ";
        std::cout << "\n    ";
        for (auto i : indices(n))
            std::cout << std::setw(2) << i << " ";
        std::cout << "\n";

        auto next_belongs_to = std::vector<set>(n, set::outer);
        next_belongs_to[order[0]] = set::border;
        auto next_to_move = std::vector<int>{};
        for (auto order_idx : indices<size_t>(order.size())) {
            auto to_add = order[order_idx];
            auto next_to_add = order_idx + 1 < n ? order[order_idx + 1] : -1;
            auto prev = std::move(cur);
            cur = step{ .belongs_to = next_belongs_to };

            std::cout << std::setw(2) << to_add << ": ";
            for (auto i : indices(cur.belongs_to.size())) {
                if (cur.belongs_to[i] == prev.belongs_to[i]) {
                    if (cur.belongs_to[i] == set::outer)
                        std::cout << " _ ";
                    if (cur.belongs_to[i] == set::border)
                        std::cout << " B ";
                    if (cur.belongs_to[i] == set::inner)
                        std::cout << " I ";
                    continue;
                }
                if (cur.belongs_to[i] == set::outer)
                    std::cout << " _ ";
                if (cur.belongs_to[i] == set::border)
                    std::cout << " A ";
                if (cur.belongs_to[i] == set::inner)
                    std::cout << " M ";
            }
            std::cout << "\n";

            auto next_border_size = size_t{0},
                next_to_move_size = size_t{0};
            if (next_to_add != -1) {
                std::tie(next_belongs_to, next_border_size, next_to_move_size) =
                    calculate_next(next_to_add, cur.belongs_to);
            } else {
                next_border_size = 1;
                next_to_move_size = 1;
                util::fill(next_belongs_to, set::inner);
            }

            for (auto& labeling_pair : prev.labelings)
            {
                for (auto to_add_label : indices(3))
                {
                    auto next_border_labels = std::vector<int>{};
                    auto next_to_move_labels = std::vector<int>{};
                    auto cur_border_labels = make_vector(n, -1);
                    auto label_it = labeling_pair.first.begin();
                    for (auto i : indices(n)) {
                        if (cur.belongs_to[i] == set::border) {
                            cur_border_labels[i] =
                                i != to_add
                                    ? *label_it++
                                    : to_add_label;
                            if (next_belongs_to[i] == set::inner)
                                next_to_move_labels.push_back(cur_border_labels[i]);
                        }
                        if (next_belongs_to[i] == set::border && i != next_to_add)
                            next_border_labels.push_back(cur_border_labels[i]);
                    }

                    auto& prev_labeling = labeling_pair.second;
                    for (auto i : indices(n))
                        if (cur_border_labels[i] == -1)
                            cur_border_labels[i] = prev_labeling.labels[i];

                    auto& cur_labeling = cur.labelings[next_border_labels];
                    auto& max_score = cur_labeling.score;
                    auto& max_to_add_score = cur_labeling.to_add_score;
                    auto& max_labeling = cur_labeling.labels;

                    auto to_add_score =
                        g.nodes[to_add].scores[cur_border_labels.at(to_add)];
                    for (auto e : g.nodes[to_add].edges) {
                        auto head_id = e->head->id,
                            foot_id = e->foot->id;
                        if (cur.belongs_to[head_id] != set::outer
                            && cur.belongs_to[foot_id] != set::outer) {
                            auto head_label = cur_border_labels.at(head_id);
                            auto foot_label = cur_border_labels.at(foot_id);
                            to_add_score += e->scores[head_label][foot_label];
                        }
                    }

                    if (prev_labeling.score + to_add_score
                        > max_score + max_to_add_score) {
                        max_score = prev_labeling.score;
                        max_to_add_score = to_add_score;
                        max_labeling = std::move(cur_border_labels);
                    }
                }
            }

            for (auto& labeling_pair : cur.labelings) {
                auto& labeling = labeling_pair.second;
                labeling.score += labeling.to_add_score;
            }
        }

        auto result = cur;
        for (auto& labeling : result.labelings) {
            std::cout << "la: ";
            for (auto label : labeling.second.labels) {
                std::cout << std::setw(2) << label << " ";
            }
            std::cout << "\n";
            std::cout << "sc: " << labeling.second.score << "\n\n";
        }

        std::cout << "time elapsed: " << std::to_string(util::elapsed<std::chrono::milliseconds>(start_time).count()) + "ms\n";
    }

    auto operator<<(std::ostream& o, const graph& g)
        -> std::ostream&
    {
        o << "nodes:\n";
        util::for_each(g.nodes, [&](auto& node) { o << node << "\n"; });
        o << "edges:\n";
        util::for_each(g.edges, [&](auto& edge) { o << edge << "\n"; });
        return o;
    }

} // namespace crf
