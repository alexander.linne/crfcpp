#pragma once

#include <iterator>
#include <vector>

#include "../util/util.hpp"

namespace crf {

    struct node;

    struct edge
    {
        std::vector<std::vector<double>> scores;
        node *head;
        node *foot;
    };

    struct node
    {
        auto neighbours() const
            -> cppcoro::generator<int>
        {
            for (auto& e : edges) {
                co_yield (e->head->id == id ? e->foot->id : e->head->id);
            }
        }

        int id;
        int reference_label;
        int observation;
        int assigned_label;
        std::vector<double> scores;
        std::vector<edge *> edges;
    };

    std::ostream& operator<<(std::ostream& o, edge e) {
        o << e.foot->id << " "
          << e.head->id;
        return o;
    }

    std::ostream& operator<<(std::ostream& o, node n) {
        o << n.id << " "
          << n.reference_label << " "
          << n.observation;
        return o;
    }

} // namespace crf
