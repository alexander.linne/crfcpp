#include <iostream>

#include "crf/data.hpp"

int main()
{
    // Create and initialize graph.
    auto g = crf::donut(5, 5, 3);

    auto transition_matrix = std::vector<std::vector<double>>{
        {0.67, 0.22, 0.11}, {0.167, 0.67, 0.166}, {0.11, 0.22, 0.67}};
    g.create_reference_label_random_walk(transition_matrix, 1000000);

    auto state_to_observation = std::vector<std::vector<double>>{
        {0.75, 0.25}, {0.5, 0.5}, {0.25, 0.75}};
    g.add_observation(state_to_observation);

    // Set scores.
    auto set_scores = [&](double alpha, double beta) {
        auto edge_scores = std::vector<std::vector<double>>{
            {beta,  0.0,  -beta},
            {0.0,   beta, 0.0},
            {-beta, 0.0,  beta}
        };
        util::for_each(g.edges, [&](auto& edge) {
            edge.scores = edge_scores;
        });
        auto node_scores = std::vector<std::vector<double>>{
            {alpha,  0.0, -alpha},
            {-alpha, 0.0, alpha}
        };
        util::for_each(g.nodes, [&](auto& node) {
            node.scores = node_scores[node.observation];
        });
    };
    set_scores(1.0, 1.1);

    // Compute score.
    auto set_labels_to = [&](int label) {
        for (auto& node : g.nodes)
            node.assigned_label = label;
    };
    for (auto i = 0; i < 3; ++i) {
        set_labels_to(i);
        std::cout << i << ": " << g.compute_score() << "\n";
    }

    util::for_each(g.nodes, [&](auto& node) {
        node.assigned_label = node.reference_label;
    });
    std::cout << "assigned = reference: " << g.compute_score() << "\n";
    set_scores(1.0, 0.1);
    std::cout << "assigned = reference (beta = 0.1): " << g.compute_score() << "\n";
    set_scores(1.0, 1.1);

    util::for_each(g.nodes, [&](auto& node) {
        node.assigned_label = node.observation == 0 ? 0 : 2;
    });
    std::cout << "assigned = observation: " << g.compute_score() << "\n";

    util::for_each(g.nodes, [&](auto& node) {
        node.assigned_label = std::uniform_int_distribution{0, 2}(util::mt19937);
    });
    std::cout << "random: " << g.compute_score();

    auto sum = 0.0;
    for (auto i = 0; i < 10000; ++i) {
        util::for_each(g.nodes, [&](auto& node) {
            node.assigned_label = std::uniform_int_distribution{0, 2}(util::mt19937);
        });
        sum += g.compute_score();
    }
    std::cout << " (" << sum / 10000.0 << ")\n\n";

    std::cout << "Viterbi:\n";
    crf::viterbi(g);

    return 0;
}
