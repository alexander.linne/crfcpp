#pragma once

#include <algorithm>
#include <random>

#include <cppcoro/generator.hpp>
#include <cppcoro/recursive_generator.hpp>

using std::begin;
using std::end;

namespace util {
    thread_local std::mt19937 mt19937;

    template<class Range>
    auto first(Range&& rng)
        -> decltype(auto)
    {
        return *begin(rng);
    }

    template<class Range>
    auto last(Range&& rng)
        -> decltype(auto)
    {
        return *std::prev(end(rng));
    }

    template<class Range>
    auto any(Range&& rng)
        -> bool
    {
        return begin(rng) != end(rng);
    }

    template<class Range>
    auto empty(Range&& rng)
        -> bool
    {
        return begin(rng) == end(rng);
    }

    template<class ForwardRange, class T>
    void fill(ForwardRange&& rng, const T& value)
    {
        std::fill(begin(rng), end(rng), value);
    }

    template<class IntType>
    auto indices(IntType max)
        -> cppcoro::generator<IntType>
    {
        for (auto i = IntType{0}; i < max; ++i) {
            co_yield i;
        }
    }

    template<class IntType>
    auto indices(IntType min, IntType max)
        -> cppcoro::generator<IntType>
    {
        for (auto i = min; i < max; ++i) {
            co_yield i;
        }
    }

    template<class InputRange, class OutputIt>
    constexpr
    auto copy(InputRange rng, OutputIt d_first)
        -> OutputIt
    {
        return std::copy(begin(rng), end(rng), d_first);
    }

    template<class Range, class UnaryPredicate>
    auto filter(Range&& rng, UnaryPredicate p)
    {
        return std::remove_if(begin(rng), end(rng),
            [&](auto&& e) { return !p(std::forward<decltype(e)>(e)); });
    };

    template<class Range>
    auto popcnt(Range&& rng) {
        return std::count_if(begin(rng), end(rng), [](auto v) { return !!v; });
    }

    template<class Range, class UnaryPredicate>
    auto remove_if(Range&& rng, UnaryPredicate p)
    {
        return std::remove_if(begin(rng), end(rng), p);
    };

    template<class IntType, class BoolRange>
    auto selected_indices(BoolRange&& brng)
        -> cppcoro::generator<IntType>
    {
        auto it = begin(brng);
        for (auto i = IntType{0}; it != std::end(brng); ++i, ++it) {
            if (*it) {
                co_yield i;
            }
        }
    }

    template<class Range, class OutIt, class Fn>
    auto transform(Range&& rng, OutIt out, Fn&& fn)
    {
        return std::transform(begin(rng), end(rng), out, std::forward<Fn>(fn));
    };

    template<class Range, class Fn>
    auto for_each(Range&& rng, Fn&& fn)
    {
        return std::for_each(begin(rng), end(rng), std::forward<Fn>(fn));
    };

    template<class Container, class Range>
    auto as_(Range&& rng)
        -> Container
    {
        auto container = Container{};
        for_each(rng, [&](auto& v) { container.push_back(v); });
        return container;
    };

    template<class Range>
    auto weighted_random_index(Range&& rng)
    {
        auto urd_val = std::uniform_real_distribution{0.0, 1.0}(mt19937);
        auto it = begin(rng);
        auto it_end = end(rng);
        for (; it != it_end; ++it) {
            urd_val -= *it;
            if (urd_val < 0)
                break;
        }
        return std::distance(begin(rng), it);
    }

    template<class Size>
    auto random_index(Size size)
        -> Size
    {
        return std::uniform_int_distribution<Size>{0, size - 1}(mt19937);
    };

    template<class IntType>
    auto ints_cross_product(std::vector<IntType> max)
        -> cppcoro::generator<const std::vector<IntType>&>
    {
        auto n = max.size();
        auto elem = std::vector<int>(n, 0);
        co_yield elem;

        if (n <= 0)
            co_return;

        do {
            elem[0]++;
            for (auto i = 0; elem[i] > max[i] && i < n-1; ++i) {
                elem[i+1]++;
                elem[i] = 0;
            }
            co_yield elem;
        } while(elem != max);
    }

    template<class T, class Range, class... Ranges>
    auto concat(Range&& rng, Ranges&&... rngs)
        -> cppcoro::recursive_generator<T>
    {
        for (auto& e : rng)
            co_yield e;
        if constexpr (sizeof...(rngs) > 0)
            co_yield concat<T>(std::forward<Ranges>(rngs)...);
    };

    auto now()
        -> std::chrono::time_point<std::chrono::system_clock>
    {
        return std::chrono::system_clock::now();
    }

    template<class Duration>
    auto elapsed(std::chrono::time_point<std::chrono::system_clock> tp)
        -> Duration
    {
        return std::chrono::duration_cast<Duration>(now() - tp);
    }

}
